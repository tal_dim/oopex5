package clids.ex5.crosswords;

/**
 * This class represent one square in the board of the crossword.
 * @author Ido Bronstein and Tal Dim
 */
public class MyCrosswordSquare {
    private char myChar;
    private boolean isVertical;
    private boolean isHorizontal;
    private boolean isPossibleToput;
    private int numberOfChars;
    public static final char startingChar='_';
    
    /**
     * Constructed that build square. If this square of the type '_' the vertical
     * and the horizontal is true, if the type is '#' the vertical
     * and the horizontal is false. The char of square initialized to '_'.
     * @param boolean if the square of the type '_' than true, else false.
     */
    public MyCrosswordSquare(boolean isPossibleToput) {
        this.myChar = startingChar;
        this.isPossibleToput = isPossibleToput;
        if(isPossibleToput) {
            this.isVertical = true;
            this.isHorizontal = true;            
        } else {   
            this.isVertical = false;
            this.isHorizontal = false;
        }
        this.numberOfChars = 0;
    }
    
    /**
     * Set the char and increase the number of chars. 
     * @param newChar 
     */
    public void setChar(char newChar) {
        myChar = newChar;
        numberOfChars++;
    }
    
    /**
     * Increase the number of chars. 
     */
    public void addChar() {
        numberOfChars++;
    }
    /**
     * Decrease the number of chars, if the number of chars is zero, 
     * set the char to '_'.
     */
    public void decChar() {
        numberOfChars--;
        if(numberOfChars==0)
            myChar=startingChar;
    }
     
    /**
     * Set the if there is a term that start from this square in vertical
     * @param newVertical 
     */
    public void setVertical(boolean newVertical) {
        isVertical = newVertical;
    }
    
   /**
     * Set the if there is a term that start from this square in horizontal
     * @param newVertical 
     */
    public void setHorizontal(boolean newHorizontal) {
        isHorizontal = newHorizontal;
    }
    
    /**
     * Get the char of the square
     * @return char
     */
    public char getChar() {
        return myChar;
    }
    
    /**
     * Tell from which type the square
     * @return true if the type is '_' and false if the type is '#'
     */
    public boolean getIsPossibleToput() {
        return isPossibleToput;
    }
    
    /**
     * Tell if there is a term that start from this square in vertical
     * @return true if there is
     */
    public boolean isVertical() {
        return isVertical;
    }
    
        /**
     * Tell if there is a term that start from this square in horizontal
     * @return true if there is
     */
    public boolean isHorizontal() {
    return isHorizontal;
    }
    
    /**
     * Get a copy of this square
     * @return copy
     */
    public MyCrosswordSquare getCopy(){
        MyCrosswordSquare temp = new MyCrosswordSquare(true);
        temp.myChar = this.myChar;
        temp.isHorizontal=this.isHorizontal;
        temp.isVertical=this.isVertical;
        temp.numberOfChars=this.numberOfChars;
        return temp;
    }
}
