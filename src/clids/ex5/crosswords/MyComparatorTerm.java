package clids.ex5.crosswords;
import java.util.Comparator;
/**
 * This class implements comparator and compere between 2 String by there length.
 * If their are equal it's compere them by abs.
 * 
 * @author Ido Bronstein and Tal Dim
 */
public class MyComparatorTerm implements Comparator<String>  {
    
    /**
     * Compere 2 String by there length, abs in this order of importance.
     * @param term1
     * @param term2
     * @return If term1 bidder than terms2 it's return -1. If they are equal 
     * it's return 0. else it return 1.
     */
    public int compare(String term1, String term2) {
        if (term1.length() > term2.length()) {
            return -1;
        } else if (term1.length() < term2.length()) {
            return 1;
        } else {
            if(term1.compareTo(term2) > 0) {
                return 1;
            } else if (term1.compareTo(term2) < 0) {
                return -1;
            }
        }
        return 0;
    }
}
