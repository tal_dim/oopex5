package clids.ex5.crosswords;

import java.util.Comparator;

/**
 * This class implements comparator and compere between 2 MyCrosswordPosition by
 * size. If their are equal it's compere them by X coordinate. If their are
 * equal it's compere them by Y coordinate. If their are equal the position that
 * are vertical is first;
 *
 * @author Ido BronStein and Tal Dim
 */
public class MyComparatorPosition implements Comparator<MyCrosswordPosition> {

    /**
     * Compere 2 MyCrosswordPosition by size, X coordinate, Y coordinate, 
     * isVertical in this order of importance.
     * @param MyCrosswordPosition1
     * @param MyCrosswordPosition2
     * @return If MyCrosswordPosition1 bigger than MyCrosswordPosition2 it 
     * return -1. Else It return 1. It assumes that they aren't equal.
     */
    public int compare(MyCrosswordPosition pos1, MyCrosswordPosition pos2) {
        if (pos1.getsize() > pos2.getsize()) {
            return -1;
        } else if (pos1.getsize() < pos2.getsize()) {
            return 1;
        } else {
            if (pos1.getX() < pos2.getX()) {
                return -1;
            } else if (pos1.getX() > pos2.getX()) {
                return 1;
            } else {
                if (pos1.getY() < pos2.getY()) {
                    return -1;
                } else if (pos1.getY() > pos2.getY()) {
                    return 1;
                } else {
                    if (pos1.isVertical()) {
                        return -1;
                    } else {
                        return 1;
                    }
                }
            }
        }
    }
}
