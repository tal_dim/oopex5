package clids.ex5.crosswords;

/**
 * This class implements CrosswordEntry and represent an entry to MyCrossword.
 * @author Ido Bronstein and Tall Dim
 */
public class MyCrosswordEntry implements CrosswordEntry{
    private MyCrosswordPosition pos;
    private String def;
    private String term;

    /**
     * Constructed that build MyCrosswordEntry object
     * @param MyCrosswordPosition
     * @param String of definition of the term
     * @param term 
     */
    public MyCrosswordEntry(MyCrosswordPosition pos, String def, String term) {
        this.pos = pos;
        this.def = def;
        this.term = term.toLowerCase();
    }
    
    /**
     * Get the definition of the term
     * @return the definition
     */
    @Override
    public String getDefinition() {
        return def;
    }

    /**
     * Get the length of the term
     * @return length
     */
    @Override
    public int getLength() {
        return term.length();
    }

   /**
     * Get the position of the entry
     * @return Position
     */
    @Override
    public CrosswordPosition getPosition() {
        return pos;
    }

    /**
     * Get the term
     * @return term
     */
    @Override
    public String getTerm() {
        return term;
    }
    
}
