package clids.ex5.crosswords;

/**
 * This class implements CrosswordPosition. Represent specifies a basic position
 * of an entry in a crossword.
 * 
 * @author Ido Bronstein and Tal Dim
 */
public class MyCrosswordPosition implements CrosswordPosition{
    private int x;
    private int y;
    private boolean vertical;
    private boolean inserted;
    private int size;

    /**
     * Constructed that build MyCrosswordPosition object
     * @param inserted
     * @param x coordinate
     * @param y coordinate
     * @param vertical
     * @param size 
     */
    public MyCrosswordPosition(boolean inserted, int x, int y, boolean vertical,
            int size) {
        this.inserted = false;
        this.size=size;
        this.x = x;
        this.y = y;
        this.vertical = vertical;
    }
    
    /**
     * Get the X coordinate
     * @return X coordinate
     */
    @Override
    public int getX() {
        return x;
    }

   /**
     * Get the Y coordinate
     * @return Y coordinate
     */
    @Override
    public int getY() {
        return y;
    }

    /**
     * Check if the position is vertical
     * @return true if it's vertical
     */
    @Override
    public boolean isVertical() {
        return vertical;
    }
    
    /**
     * Turn the position to true
     */
    public void insert(){
        this.inserted=true;
    }
    
    /**
     * Turn the position to false
     */
    public void extract(){
        this.inserted=false;
    }
    
    /**
     * Get if the position is inserted 
     * @return true if it's inserted
     */
    public boolean isInserted(){
        return inserted;
    }
    
    /**
     * Get the size of the portion
     * @return size
     */
    public int getsize(){
        return size;
    }
    
}
