package clids.ex5.crosswords;

import java.util.Iterator;
import java.util.TreeSet;

/**
 * This class implements Iterator and represent a collection of CrosswordEntry
 *
 * @author Ido Brondtein and Tal Dim
 */
public class MyIterator implements Iterator<CrosswordEntry> {

    private String[] terms;
    private MyCrosswordPosition[] positions;
    private int i, j; //Indexs the run on the arrays. i for positions and j for terms
    private MyCrossword crossword; //To check if position is fit
    private MyCrosswordDictionary dictionary; //To get the def of every term 

    /**
     * Constructed that get tree set of terms and position and set them to array
     * according to specific order.
     *
     * @param crossword
     * @param dictionary
     * @param positions
     * @param terms
     */
    public MyIterator(MyCrossword crossword, MyCrosswordDictionary dictionary, 
            TreeSet<MyCrosswordPosition> positions, TreeSet<String> terms) {
        //Put all the terms and the positions to tree set that buuild with specific 
        //comparator that order them.
        TreeSet<String> treeTerms = new TreeSet<>(new MyComparatorTerm());
        Iterator<String> allTerms = terms.iterator();
        while (allTerms.hasNext()) {
            treeTerms.add(allTerms.next());
        }
        this.terms = treeTerms.toArray(new String[treeTerms.size()]);
        this.positions = positions.toArray(new MyCrosswordPosition[positions.size()]);
        this.crossword = crossword;
        this.dictionary = dictionary;
        i = 0;
        j = 0;

    }

    /**
     * Check if there is a another entry.
     *
     * @return true if there is
     */
    public boolean hasNext() {
        //Continue to run until there is no positions or it's find a position that
        // fit to the bord
        while (true) {
            if (isEnd()) {
                return false;
            }
            // If the positions is inserted go to the next position with all the terms.
            if (positions[i].isInserted()) {
                i++;
                j = 0;
            // Check if ther is a word that fit to this position
            } else if (!(crossword.canEnterTo(terms[j], positions[i]))) {
                advance();
            } else {
                //If there is stop the while
                break;
            }
        }
        return true;
    }

    /*
     * Get the next word. If there is no word take the next position with all the 
     * words.
     */
    private void advance() {
        if (j == terms.length - 1) {
            i++;
            j = 0;
        } else {
            j++;
        }
    }

    /*
     * Check if there is next postion.
     */
    private boolean isEnd() {
        return i == positions.length;
    }

    /**
     * Get the next entry that fit to the board.
     * @return MyCrosswordEntry
     */
    public MyCrosswordEntry next() {
        positions[i].insert();
        MyCrosswordEntry entry = new MyCrosswordEntry(positions[i], 
                dictionary.getTermDefinition(terms[j]), terms[j]);
        advance(); //Set to the index to the next word
        return entry;
    }

    /**
     * Remove the next entry.,
     */
    public void remove() {
        next();
    }
}
