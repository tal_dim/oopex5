package clids.ex5.crosswords;

import clids.ex5.search.SearchBoard;
import java.util.Collection;
import java.util.Iterator;
import java.util.TreeSet;
import java.util.ArrayList;

/**
 * This class represent a crossword puzzle. 
 *
 * @author Ido Bronstein and Tal Dim
 */
public class MyCrossword implements Crossword {

    private CrosswordDictionary dictionary;
    private CrosswordStructure structure;
    private MyCrosswordSquare[][] board;
    private TreeSet<String> terms;
    private TreeSet<MyCrosswordPosition> positions;
    private int quality;
    private ArrayList<CrosswordEntry> entries;

    /**
     * Default constructor of MyCrosword.
     */
    public MyCrossword() {
        this.entries = new ArrayList<>();
        this.terms = new TreeSet<String>();
        this.positions = new TreeSet<MyCrosswordPosition>();
        this.quality = 0;
    }

    /**
     * Add all the terms and the definition to this MyCrossword
     *
     * @param dictionary
     */
    @Override
    public void attachDictionary(CrosswordDictionary dictionary) {
        this.dictionary = dictionary;
        this.terms = new TreeSet<>(dictionary.getTerms());

    }

    /**
     * Create a board of MyCrosswordSquare from a structure.
     *
     * @param structure
     */
    @Override
    public void attachStructure(CrosswordStructure structure) {
        this.structure = structure;
        //Create 2-D array of MyCrosswordSquare that represent the bord of the 
        //game
        this.board = new MyCrosswordSquare[structure.getHeight()][this.structure.
                getWidth()];
        for (int i = 0; i < structure.getHeight(); i++) {
            for (int j = 0; j < structure.getWidth(); j++) {
                MyCrosswordPosition position = new MyCrosswordPosition(true, j,
                        i, false, 0);
                //Check if the squere is '_' or '#'
                if (structure.getSlotType(position) == CrosswordStructure.
                        SlotType.UNUSED_SLOT) {
                    this.board[i][j] = new MyCrosswordSquare(true);
                } else if (structure.getSlotType(position) == CrosswordStructure.
                        SlotType.FRAME_SLOT) {
                    this.board[i][j] = new MyCrosswordSquare(false);
                } else {
                    throw new IndexOutOfBoundsException();
                }
            }
        }
        createPositionsToPutTree();

    }

    /*
     * This method create all the position that can be in ths board and put them
     * on a tree that organize them by MyComparatorPosition.
     */
    private void createPositionsToPutTree() {
        this.positions = new TreeSet<>(new MyComparatorPosition());
        //Pass on every square
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[0].length; j++) {
                //Create Horizontal position
                int size = getSizeRight(i, j);
                if (size >= 1) {
                    this.positions.add(new MyCrosswordPosition(false, j, i, false
                            , size));
                }
                //Create Vertical position 
                size = getSizeDown(i, j);
                if (size >= 1) {
                    this.positions.add(new MyCrosswordPosition(false, j, i, true,
                            size));
                }
            }
        }
    }

    /**
     * Get the size of the Horizontal position in (X,Y) on the board
     *
     * @param y coordinate
     * @param x coordinate
     * @return size of the position
     */
    private int getSizeRight(int y, int x) {
        if (!board[y][x].isVertical()) {
            return 0;
        }
        int i = 1;
        int max = board[0].length;
        //Increse i until it get to '#' or '_' 
        while (x + i < max) {
            if (!board[y][x + i].getIsPossibleToput()) {
                return i;
            }
            i++;
        }
        return i;
    }

    /**
     * Get the size of the Vertical position in (X,Y) on the board
     *
     * @param y coordinate
     * @param x coordinate
     * @return size of the position
     */
    private int getSizeDown(int y, int x) {
        if (!board[y][x].isHorizontal()) {
            return 0;
        }
        int i = 1;
        int max = board.length;
        //Increse i until it get to '#' or '_' 
        while (y + i < max) {
            if (!board[y + i][x].getIsPossibleToput()) {
                return i;
            }
            i++;
        }
        return i;
    }

    /**
     * Add entry to the board
     *
     * @param move this entry
     */
    @Override
    public void doMove(CrosswordEntry move) {
        //Remove the term from the list of the terms that it's still need to put
        terms.remove(move.getTerm());
        String s = move.getTerm();
        //Create position and set him to be insert.
        MyCrosswordPosition p = (MyCrosswordPosition) move.getPosition();
        p.insert();
        this.entries.add(move);
        int x = p.getX();
        int y = p.getY();
        boolean vertical = p.isVertical();
        //Set the board to be includ the new chars and the square that the word 
        //start from him to insert in the right derction.
        if (vertical) {
            board[y][x].setVertical(true);
        } else {
            board[y][x].setHorizontal(true);
        }
        for (int i = 0; i < s.length(); i++) {
            if (vertical) {
                board[y + i][x].setChar(s.charAt(i));
            } else {
                board[y][x + i].setChar(s.charAt(i));
            }
            quality++;
        }
    }

    /**
     * Get copy of the crossword
     *
     * @return copy
     */
    @Override
    public SearchBoard<CrosswordEntry> getCopy() {
        //Take reference from all the needed flieds.
        MyCrossword toBeReturned = new MyCrossword();
        toBeReturned.dictionary = this.dictionary;
        toBeReturned.structure = this.structure;
        // If the board is empty try this
        try {
            toBeReturned.board = new MyCrosswordSquare[board.length][board[0].length];
            for (int i = 0; i < board.length; i++) {
                for (int j = 0; j < board[0].length; j++) {
                    toBeReturned.board[i][j] = this.board[i][j].getCopy();
                }
            }
        } catch (Exception e) {
        }
        //Else keep do refernce
        toBeReturned.quality = this.quality;
        toBeReturned.terms = copyTree(this.terms);
        toBeReturned.entries = new ArrayList<>();
        for (CrosswordEntry a1 : this.entries) {
            toBeReturned.entries.add(a1);
        }
        return toBeReturned;
    }

    /**
     * Get all the entries that the crossword contains
     *
     * @return all the entries
     */
    @Override
    public Collection<CrosswordEntry> getCrosswordEntries() {
        return entries;
    }

    /**
     * Get all the possible entries that can fit to the next move.
     *
     * @return all possible entries
     */
    @Override
    public Iterator<CrosswordEntry> getMovesIterator() {
        return new MyIterator(this, (MyCrosswordDictionary) dictionary, positions,
                terms);
    }

    /**
     * Get the quality of the board
     *
     * @return quality
     */
    @Override
    public int getQuality() {
        return this.quality;
    }

    /**
     * Get the quality bound of the board
     *
     * @return quality bound
     */
    @Override
    public int getQualityBound() {
        Iterator<MyCrosswordPosition> pos = this.positions.iterator();
        TreeSet<String> newTree = copyTree(this.terms);
        int QU = this.quality;
        //Pass on all the popositions and check if there inserted
        while (pos.hasNext()) {
            MyCrosswordPosition position = pos.next();
            Iterator<String> words = newTree.iterator();
            if (!position.isInserted()) {
                //Pass on all the words and check if they can enter to this board.
                while (words.hasNext()) {
                    String a1 = words.next();
                    if (canEnterTo(a1, position)) {
                        words.remove();
                        QU += a1.length();
                    }
                }
                if (newTree.isEmpty()) {
                    return QU;
                }
            }
        }
        return QU;
    }

    /**
     * Return if this board is the best solution
     *
     * @return true if this the best solution
     */
    @Override
    public boolean isBestSolution() {
        return this.terms.isEmpty();
    }

    /**
     * Take out a entry from the board
     *
     * @param move
     */
    @Override
    public void undoMove(CrosswordEntry move) {
        ////Add the term from the list of the terms that it's still need to put
        terms.add(move.getTerm());
        String s = move.getTerm();
        MyCrosswordPosition p = (MyCrosswordPosition) move.getPosition();
        this.entries.remove(move);
        int x = p.getX();
        int y = p.getY();
        boolean vertical = p.isVertical();
        //Set the board to be unInclud the new chars and the square that the word 
        //start from him to not insert in the right derction.
        if (vertical) {
            board[y][x].setVertical(false);
        } else {
            board[y][x].setHorizontal(false);
        }
        for (int i = 0; i < s.length(); i++) {
            if (vertical) {
                board[y + i][x].decChar();
            } else {
                board[y][x + i].decChar();
            }
            quality--;
        }
        p.extract();
    }

    /**
     * Check if a string can enter to a specific position
     *
     * @param String
     * @param MyCrosswordPosition
     * @return true if it fit, else false.
     */
    public boolean canEnterTo(String s, MyCrosswordPosition pos) {
        try {
            boolean vertical = pos.isVertical();
            int x = pos.getX();
            int y = pos.getY();
            MyCrosswordSquare help = null;
            //Pass on the board in the coordinates of the position and check if
            //the board fit to the string.
            for (int i = 0; i < s.length(); i++) {
                if (vertical) {
                    help = board[y + i][x];
                } else {
                    help = board[y][x + i];
                }
                if (help.getChar() != s.charAt(i) && help.getChar() != 
                        MyCrosswordSquare.startingChar) {
                    return false;
                }
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /*
     * Take copy of the TreeSet tha contains the terms.
     */
    private TreeSet<String> copyTree(TreeSet<String> a) {
        if (a == null) {
            return null;
        }
        TreeSet<String> temp = new TreeSet<>();
        Iterator<String> k = a.iterator();
        while (k.hasNext()) {
            temp.add(k.next());
        }
        return temp;
    }
}
