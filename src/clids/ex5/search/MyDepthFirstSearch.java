package clids.ex5.search;

import java.util.Iterator;
import java.util.Date;

/**
 * Do s search in the system of DFS. It's implements DepthFirstSearch and Work
 * with B that stands for a concrete type of SearchBoardEntry.
 *
 * @author Ido Bronstein and Tal Dim
 */
public class MyDepthFirstSearch<B extends SearchBoard<M>, M extends BoardMove> 
implements DepthFirstSearch<B, M> {

    private B BestYet;
    private int quality;
    private long startTime;
    private long timeOut;

    /**
     * Performs Depth-First search, up to depth maxDepth, and using at most
     * timeOut ms, starting from the given SearchBoardEntry.
     *
     * @param startBoard - the board to start from him
     * @param maxDepth - max depth for the search
     * @param timeOut - max time to the search that in the end of him it stop
     * @return the SearchBoardEntry with the highest quality.
     */
    @Override
    public B performSearch(B startBoard, int maxDepth, long timeOut) {
        startTime = new Date().getTime();
        setBest(startBoard);
        this.timeOut = timeOut;
        try {
            performSearch1(startBoard, maxDepth);
        } catch (timeOutException | doneException e) {
        }
        return BestYet;
    }

    /*
     * Do the sreach
     */
    private void performSearch1(B startBoard, int maxDepth) throws 
            timeOutException, doneException {
        if (pastTime()) { //If the time is over go out
            throw new timeOutException();
        }
        if (maxDepth == -1) { //If maxDepth negative stop search 
            return;
        }
        //
        if (startBoard.getQualityBound() <= quality) {  
            return;                                    
        }         
        //
        if (startBoard.getQuality() > quality) {
            setBest(startBoard);
        }
        if (startBoard.isBestSolution()) { //If this is the best solution get out
            throw new doneException();     // With this board
        }
        Iterator<M> places = startBoard.getMovesIterator();
        //
        while (places.hasNext()) {
            M move = places.next();
            startBoard.doMove(move);
            performSearch1(startBoard, maxDepth - 1);
            startBoard.undoMove(move);
        }

    }

    /*
     * Get a copy of B and the quality that he start with.
     */
    private void setBest(B startBoard) {
        this.BestYet = (B) startBoard.getCopy();
        this.quality = BestYet.getQuality();
    }

    /**
     * Check if the time is pass
     * @return true if it pass
     */
    private boolean pastTime() {
        return (new Date().getTime() - startTime > timeOut);
    }
}
