/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package clids.ex5.search;

/**
 * Exception that throw when the DFS search longer than the timeOut
 *
 * @author Ido Bronstein and Tal Dim
 */
public class timeOutException extends Exception {

    private static final long serialVersionUD = 1L;


    /**
     * Default constructor who inherit the exception class.
     */    
    public timeOutException() {
    }
}
