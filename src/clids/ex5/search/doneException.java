/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package clids.ex5.search;

/**
 * Exception that throw when the program arrived to the best solution and help
 * to to close the search
 *
 * @author Ido Bronstrin and Tal Dim
 */
public class doneException extends Exception {

    private static final long serialVersionUD = 1L;

   /**
     * Default constructor who inherit the exception class.
     */ 
    public doneException() {
    }
}
